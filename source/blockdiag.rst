blockdiag
==========


.. blockdiag::

   blockdiag {
     blockdiag -> generates -> "block-diagrams";
     blockdiag -> is -> "very easy!";

     blockdiag [color = "greenyellow"];
     "block-diagrams" [color = "pink"];
     "very easy!" [color = "orange"];
   }


日本語(Japanese)
----------------

.. blockdiag::

   blockdiag {
     blockdiag -> generates -> "ブロック";
     blockdiag -> is -> "簡単だよ!";

     blockdiag [color = "greenyellow"];
     "ブロック" [color = "pink"];
     "簡単だよ!" [color = "orange"];
   }



.. note::

  Blockdiag Official Dcument is `here <http://blockdiag.com/en/index.html/>`_